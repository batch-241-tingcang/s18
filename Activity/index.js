/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.
*/

		function printSum(addends1, addends2) {
			console.log ("Displayed sum of " + addends1 + " and " + addends2)
		}

		printSum(5, 15);
		console.log(20);


/*
	Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.
*/

		function printDifference(difference1, difference2) {
			console.log ("Displayed difference of " + difference1 + " and " + difference2)
		}

		printSum(20, 5);
		console.log(15);

/*
		-invoke and pass 2 arguments to the addition function
*/
		

/*
		-invoke and pass 2 arguments to the subtraction function
*/

		

/*

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

*/

		function printProduct(product1, product2) {
			return ("The product of " + product1 + " and " + product2 + ":");
		
		}
		let product = printProduct(50, 10);
		console.log(product);
		console.log(500);

/*			

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

*/
		function printQuotient(quotient1, quotient2) {
			return ("The product of " + quotient1+ " and " + quotient2 + ":");
		
		}
		let quotient = printQuotient(50, 10);
		console.log(quotient);
		console.log(5);


/*

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.


/*
	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

*/

		function returnAreaCircle(radius) {

			console.log("The result of getting the area of a circle within " + radius + " radius:")
			return 3.14 * (radius ** 2)

		}

		let circleArea = returnAreaCircle(15)
		console.log(circleArea)
		

/*
	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	
*/
		function returnAverage(num1, num2, num3, num4) {
			console.log("The average of " + num1 + ", " + num2 + ", " + num3 + " and " + num4 + ":")
			return (num1 + num2 + num3 + num4) / 4
		}	

		let averageVar = returnAverage(20, 40, 60, 80);
		console.log(averageVar)

/*
	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

		function returnPassingScore(yourScore, totalScore) {
			console.log("Is " + yourScore + "/" + totalScore + " a passing score?")
			return (yourScore / totalScore) * 100
		}

		let isPassingScore = returnPassingScore(38, 50)
		
		console.log(isPassingScore > 75)
















