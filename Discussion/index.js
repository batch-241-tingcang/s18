// console.log("Hello!")

/*
	You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the funciton
*/

/*
	Syntax: 

	function functionnName(parameter) {
		code block
	}

	functionName(argument)
*/

// name is called parameter
// "parameter" acts as a named variable/container that exists only inside of a function
// it is used to store information that is provided to a function when it is called/invoked

// Variable can be a parameter

let name = "John";

function printName(name) {
	console.log("My Name is " + name);
}

printName()

// "Juana" and "Mica", the information/data provided directly into the function, it is called an argument.
printName("Juana");
printName("Mica");

let userName = "Elaine";
printName(userName);

// Parameter is just the container
// Argument is the data that is stored
// Variables can also be passed as an ARGUMENT
// Si parameter ang priority than global variable

function greeting() {
	console.log("Hello, User!")
}

greeting("Eric");

// Walang lugar si Eric


// ----------

// Using Multiple Parameters
//Multiple "arguments" will corresponds to the number of "parameters" declared in a function in succeeding order.
//Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.
//The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.


function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Erven", "Joshua", "Cabral");
// "Erven" will be stored in the parameter "firstName"
// "Joshua" will be stored in the parameter "middleName"
// "Cabral" will be stored in the parameter "lastName"


/*
	In JS, providing more/less arguments than the expected parameters will not return an error

	In other programming languages, this will return an error stating that "the expected number of argument do not match the number of parameters".


*/
createFullName("Eric", "Andales"); //pag kulang ng argument, undefined
createFullName("Roland", "John", "Doroteo", "Jane") //pag sobrang argument, di lumalabas

// ----------

function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is " + remainder);
	let isDivisibleBy8 = (remainder === 0);
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);


// ----------

/*
	Mini Activity:

	1. Create a function which is able to receive data as an argument.

		- This function should be able to receive the name of your favorite superhero
		- Display the name of your favorite superhero in the console.


	Kindly send the ss in our gc.

*/

function myFavoriteSuperhero(superheroName) {
	console.log("My favorite superhero is " + superheroName)
}

myFavoriteSuperhero("Anya");


// ----------


// Return Statement
/*
	The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
*/

function returnFullName(firstName, middleName, lastName) {

	return firstName + " " + middleName + " " + lastName

	console.log("Can we print this message?");
}

// Return = kinukuha lang yung value ni fname, mname, lname


// Whatever value that is returnedfrom the "returnFullName" function can be stored in a variable

let completeName = returnFullName("John","Doe", "Smith");
console.log(completeName);
console.log(returnFullName(("John","Doe", "Smith")));

completeName = returnFullName("Jeru", "Nebur", "Palma");
console.log(completeName);

// istore sa variable ang result ng function
// parang naging variable narin yung function na may value
// all codes below return will not be executed (console.log("Can we print this message?"))
// si return statement ay placed at the end of the function
// si console.log ay just for debugging
//  1 function, 1 job = Single Responsibility Principle

// ----------

function returnAddress(city, country) {

	let fullAddress = city + ", " + country;
	return fullAddress
}

let myAddress = returnAddress("Sta. Mesa, Manila", "Philippines");
console.log(myAddress);

//  Consider the following code.


/*
	Mini Activity:
	1. Debug our code.
*/


function printPlayerInformation(userName, level, job) {

	

	 console.log("Username: " + userName);
	 console.log("Level: " + level);
	 console.log("Job: " + job);

	 return userName + " " + level + " " + job
}

let user1 = printPlayerInformation("cardo123", "999", "Immortal");
console.log(user1);

// si console.log taga publish lang sa console ng code
// returns undefined because printPlayerInformation returns nothing. It onlylogs the message in the console.
// You cannot save any value from printPlayerInformation() because it does not RETURN anything
// wala nasave sa memory pag walang return, nasa console lang


// Other Ways

// function printPlayerInformation(userName, level, job) {

// 	 // return "Username: " userName + ", Level: " + level + ", Job: " + job

// 	// let userInfo = "Username: " userName + ", Level: " + level + ", Job: " + job
// 	// return userInfo
// }

// let user1 = printPlayerInformation("cardo123", "999", "Immortal");
// console.log(user1);


// ----------




















